
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import Paginator from '../../components/Paginator';
import TabPanel from '../../components/TabPanel';
import { Subject } from '../../models';
import { Page } from '../../models/page.dto';
import fetchSubjects  from '../../services/subjects-service';
// import { getSubject } from '../../services/subjects-service';

const SubjectList = (props: any) => {
  
    const [page, setPage] = useState(1)
    const [lastPage, setLastPage] = useState(0)
    const [size, setSize] = useState(2)
    const [subjects, setSubjects] = useState([] );
    useEffect(() => {
        (
            async () => {

                //  const { data } = await axios.get<Page<Subject[]>>(`subject/page?page=${page}&size=${size}`);
                const content   = fetchSubjects(page,size);
                console.log(content);
                
                  setSubjects(content);
                    // console.log(data);
                    
                  setLastPage(content.lastPage - 1)//PRoveriti da li je ovo stvarno last page

            }
        )()
    }, [page, size])

    const handlePage = (page: number, size: number) => {

        setPage(page);
        setSize(size);
        if(size  > subjects.length){
            setPage(0);
        }
    }

    return (
        <TabPanel value={props.value} index={props.index}>

            <Paginator page={page} lastPage={lastPage} size={size} pageChanged={handlePage} />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col" >id</th>
                        <th scope="col" >Name</th>
                        <th scope="col">ESPB</th>
                        <th scope="col">Year of Study</th>
                        <th scope="col" >Semester</th>
                        <th scope="col" >Actions</th>

                    </tr>

                    
                </thead >
                <tbody>
                    {subjects.map((s: Subject, i: number) => {
                        return (
                            <tr key={s.id}>
                                <td>{i + 1}</td>
                                <td>{s.id}</td>
                                <td>{s.name}</td>
                                <td>{s.noOfEsp}</td>
                                <td>{s.yearOfStudy}</td>
                                <td>{s.semester}</td>

                                <td>
                                    <div className="btn-group mb-3">

                                        <button type="button" className="btn btn-outline-secondary">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="blue" className="bi bi-info-circle" viewBox="0 0 16 16">
                                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                                            </svg>
                                            <span className="visually-hidden">Button</span>
                                        </button>
                                        <button type="button" className="btn btn-outline-secondary">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil" viewBox="0 0 16 16">
                                                <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z" />
                                            </svg>
                                            <span className="visually-hidden">Button</span>
                                        </button>
                                        <button type="button" className="btn btn-outline-secondary">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" className="bi bi-trash" viewBox="0 0 16 16">
                                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
                                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"></path>
                                            </svg>
                                            <span className="visually-hidden">Button</span>
                                        </button>
                                    </div>

                                </td >
                            </tr>


                        )
                    })}




                </tbody >
            </table >
          

        </TabPanel >

    );
};

export default SubjectList;