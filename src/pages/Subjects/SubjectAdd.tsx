import React, { SyntheticEvent, useState } from 'react';
import TabPanel from '../../components/TabPanel';
import { Semester } from '../../models';




const SubjectAdd = (props: any) => {
    const { children, value, index, ...other } = props;// {} postvaljanje podataka unutar viticastih je destruktuiranje podataka 
    const [name,setName] = useState('' as string);
    const [description,setDescription] = useState('' as string);
    const [espb,setEspb] = useState(0 as number);
    const [yor,setYor] = useState(0 as number)
    const [semster,setSemeter] = useState('' as Semester)

    const handleSubmit = (e:SyntheticEvent)=>{

    }
    return (
        <TabPanel value={value} index={1}>

            <form onSubmit={handleSubmit}>

                <div className="form-group">
                    <label htmlFor="description">Name</label>
                    <input type="text" className="form-control" id="name" placeholder="Name" />
                </div>
                <div className="form-group">
                    <label htmlFor="name">Description</label>
                    <textarea className="form-control" id="description" placeholder="Description" />
                </div>
                <div className="form-group">
                    <label htmlFor="description">ESPB</label>
                    <input type="number" className="form-control" id="espb" placeholder="ESPB" />
                </div>
                <div className="form-group">
                    <label htmlFor="description">Year of study</label>
                    <input type="number" className="form-control" id="yos" placeholder="Year of study" />
                </div>
                <div className="form-group">
                    <label htmlFor="semeter">Semester</label>
                    <select className=" form-control" id="semeter">
                        <option value={"Winter"}>Winter</option>
                        <option value={"Summer"}>Summer</option>

                    </select>
                </div>

                <div className="form-group">
                    <button type="submit" className="btn btn-primary " style={{marginTop: "10px"}}>Save Subject</button>

                </div>
            </form>
        </TabPanel>
    );
};

export default SubjectAdd;