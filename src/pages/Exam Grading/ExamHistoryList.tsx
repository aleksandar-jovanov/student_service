import React from 'react';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import TabPanel from '../../components/TabPanel';





const ExamHistoryList = (props:any) => {
    const { value, index } = props;
    return (
        <TabPanel value={props.value} index={props.index}>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col" >Id</th>
                    <th scope="col" >Exam</th>
                    <th scope="col">Examination Period</th>
                    <th scope="col">Student Index</th>
                    <th scope="col">Grade</th>
                    <th scope="col" >Actions</th>

                </tr>
            </thead >
            <tbody>
                <tr >
                    <th scope="row">#</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some Exam</td>
                    <td scope="row">Some Examination Period</td>
                    <th scope="col">Student Index</th>
                    <td scope="row">Grade</td>
                
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
                <tr >
                    <th scope="row">#</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some Exam</td>
                    <td scope="row">Some Examination Period</td>
                    <th scope="col">Student Index</th>
                    <td scope="row">Grade</td>
                
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
                <tr >
                    <th scope="row">#</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some Exam</td>
                    <td scope="row">Some Examination Period</td>
                    <th scope="col">Student Index</th>
                    <td scope="row">Grade</td>
                
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
            </tbody >
        </table >
    </TabPanel >

    );
};

export default ExamHistoryList;