import { Box, Typography } from '@material-ui/core';

import React from 'react';
import TabPanel from '../../components/TabPanel';

const ExamUngradedList = (props:any) => {
    const { children, value, index, ...other } = props;
    return (
        <TabPanel value={props.value} index={props.index}>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col" >Id</th>
                    <th scope="col" >Exam</th>
                    <th scope="col">Examination Period</th>
                    <th scope="col">Student Index</th>
                    <th scope="col">Grade</th>
                    <th scope="col" >Actions</th>

                </tr>
            </thead >
            <tbody>
                <tr >
                    <th scope="row">#</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some Exam</td>
                    <td scope="row">Some Examination Period</td>
                    <th scope="col">Student Index</th>
                    <td scope="row">Ungraded</td>
                
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
                <tr >
                    <th scope="row">#</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some Exam</td>
                    <td scope="row">Some Examination Period</td>
                    <th scope="col">Student Index</th>
                    <td scope="row">Ungraded</td>
                
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
                <tr >
                    <th scope="row">#</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some Exam</td>
                    <td scope="row">Some Examination Period</td>
                    <th scope="col">Student Index</th>
                    <td scope="row">Ungraded</td>
                
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
            </tbody >
        </table >
    </TabPanel >
    );
};

export default ExamUngradedList;