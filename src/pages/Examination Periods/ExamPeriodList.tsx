import React from 'react';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import TabPanel from '../../components/TabPanel';





const ExamPeriodList = (props:any) => {
    const { value, index } = props;
    return (
        <TabPanel value={props.value} index={props.index}>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col" >Id</th>
                    <th scope="col" >Starting Date</th>
                    <th scope="col">Ending Date</th>
                    <th scope="col">Active</th>
                   
                    <th scope="col" >Actions</th>

                </tr>
            </thead >
            <tbody>
                <tr >
                    <th scope="row">#</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some Name</td>
                    <td scope="row">Some Starting Date</td>
                    <td scope="row">Some Ending Date</td>
                    <td scope="row">Is Active</td>
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
                <tr >
                    <th scope="row">#</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some Name</td>
                    <td scope="row">Some Starting Date</td>
                    <td scope="row">Some Ending Date</td>
                    <td scope="row">Is Active</td>
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
                <tr >
                    <th scope="row">#</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some Name</td>
                    <td scope="row">Some Starting Date</td>
                    <td scope="row">Some Ending Date</td>
                    <td scope="row">Is Active</td>
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
            </tbody >
        </table >
    </TabPanel >

    );
};

export default ExamPeriodList;