import React from 'react';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import TabPanel from '../../components/TabPanel';





const ProfessorList = (props:any) => {
    const { value, index } = props;
    return (
        <TabPanel value={props.value} index={props.index}>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col" >Id</th>
                    <th scope="col" >First name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col" >Title</th>
                    <th scope="col" >Actions</th>

                </tr>
            </thead >
            <tbody>
                <tr >
                    <th scope="row">Some index</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some First Name</td>
                    <td scope="row">Some Last name</td>
                    <td scope="row">Some email</td>
                    <td scope="row">Title</td>
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
                <tr >
                    <th scope="row">Some index</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some First Name</td>
                    <td scope="row">Some Last name</td>
                    <td scope="row">Some email</td>
                    <td scope="row">Title</td>
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
                <tr >
                    <th scope="row">Some index</th>
                    <th scope="row">Id</th>
                    <td scope="row">Some First Name</td>
                    <td scope="row">Some Last name</td>
                    <td scope="row">Some email</td>
                    <td scope="row">Title</td>
                   
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
            </tbody >
        </table >
    </TabPanel >

    );
};

export default ProfessorList;