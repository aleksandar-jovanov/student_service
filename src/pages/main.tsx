import React from 'react';

import Wrapper from '../components/Wrapper';

const Main = () => {
    return (
       
       <Wrapper>
   <nav className="navbar navbar-dark sticky-top   p-1 shadow py-md-1"  style={{backgroundColor:"#6610f2"}}>
   <ul className="my-3 my-md-0 mr-md-3">
   <h1  className="h1-2 text-white text-decoration-none">Exam Application</h1>
   </ul>
       
   </nav>
           
       </Wrapper>
    );
};

export default Main;