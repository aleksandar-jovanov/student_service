import React from 'react';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import TabPanel from '../../components/TabPanel';





const StudentList = (props:any) => {
    const { value, index } = props;
    return (
        <TabPanel value={props.value} index={props.index}>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col" >Index</th>
                    <th scope="col" >First name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col" >Year of Study</th>
                    <th scope="col" >Actions</th>

                </tr>
            </thead >
            <tbody>
                <tr >
                    <th scope="row">Some index</th>
                    <td>Some First Name</td>
                    <td>Some Last name</td>
                    <td>Some email</td>
                    <td>Some Year of Study</td>
                    <td></td>
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
                <tr >
                    <th scope="row">Some index</th>
                    <td>Some First Name</td>
                    <td>Some Last name</td>
                    <td>Some email</td>
                    <td>Some Year of Study</td>
                    <td></td>
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
                <tr >
                    <th scope="row">Some index</th>
                    <td>Some First Name</td>
                    <td>Some Last name</td>
                    <td>Some email</td>
                    <td>Some Year of Study</td>
                    <td></td>
                    <td>
                        <button>
                            Edit
                        </button>
                        <button>
                            Info
                        </button>

                        <button >
                            DELETE
                        </button >

                    </td >
                </tr >
            </tbody >
        </table >
    </TabPanel >

    );
};

export default StudentList;