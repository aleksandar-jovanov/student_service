import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import App from '../../App';
import Wrapper from '../../components/Wrapper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core';
import StudentList from './StudentList';
import StudentAdd from './StudentAdd';



function a11yProps(index: any) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));
const Students = () => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const handleChange = (event: any, newValue: any) => {
        setValue(newValue);
    };
    
    return (
        <Wrapper>
            <nav className="navbar navbar-dark sticky-top   p-1 shadow py-md-1"  style={{backgroundColor:"#6610f2"}}>
   <ul className="my-3 my-md-0 mr-md-3">
        <h1  className="h1-2 text-white text-decoration-none">Students</h1>
   </ul>
       
   </nav>
            <div className={classes.root}>
                <AppBar position="static" style={{ width: "100%" }}>
                    <Tabs value={value} onChange={handleChange}>
                        <Tab label="Student List"  {...a11yProps(0)} />
                        <Tab label="Add Student"  {...a11yProps(1)} />


                    </Tabs>
                </AppBar>
                <StudentList value={value} index={0}/>
                <StudentAdd value={value} index={1}/>


            </div >


        </Wrapper >
    );
};

export default Students;

