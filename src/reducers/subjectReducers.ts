import {FETCH_SUBJECTS_PENDING,FETCH_SUBJECTS_SUCCESS,FETCH_SUBJECTS_ERROR} from '../actions/subjectActions'
import { Subject } from '../models'
import { Page } from '../models/page.dto'

const initialState = {
    pending: false,
    subjects: [] as unknown as Page<Subject[]>,
    error: null
}

export function subjectsReducer(state = initialState, action:any) {
    switch(action.type) {
        case FETCH_SUBJECTS_PENDING: 
            return {
                ...state,
                pending: true
            }
        case FETCH_SUBJECTS_SUCCESS:
            return {
                ...state,
                pending: false,
                subjects: action.payload
            }
        case FETCH_SUBJECTS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default: 
            return state;
    }
}

export const getSubjects = (state: { subjects: Page<Subject[]> }) => state.subjects;
export const getSubjectsPending = (state: { pending: any }) => state.pending;
export const getSubjectsError = (state: { error: any }) => state.error;
