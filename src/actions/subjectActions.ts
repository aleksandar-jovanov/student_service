import { Subject } from "../models";
import { Page } from "../models/page.dto";

export const FETCH_SUBJECTS_PENDING = 'FETCH_SUBJECTS_PENDING';
export const FETCH_SUBJECTS_SUCCESS = 'FETCH_SUBJECTS_SUCCESS';
export const FETCH_SUBJECTS_ERROR = 'FETCH_SUBJECTS_ERROR';

export function fetchSubjectsPending() {
    return {
        type: FETCH_SUBJECTS_PENDING
    }
}

export function fetchSubjectsSuccess(subjects: Page<Subject[]>) {
    return {
        type: FETCH_SUBJECTS_SUCCESS,
        subjects: subjects
    }
}

export function fetchSubjectsError(error:Error) {
    return {
        type: FETCH_SUBJECTS_ERROR,
        error: error
    }
}
// export default {fetchSubjectsPending,fetchSubjectsSuccess,fetchSubjectsError};

