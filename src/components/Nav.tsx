

import React from 'react';
import { Link } from 'react-router-dom';

const Nav = () => {
    return (
        <nav className="navbar navbar-dark sticky-top bg-danger  p-0 shadow py-md-4">
        {/* <a className="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">Student Service</a> */}
            
           
        <ul className="my-2 my-md-0 mr-md-3">
              
            <Link className="p-4 text-white text-decoration-none bg-danger" to="/">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
            </Link>
            <Link className="p-4 text-white text-decoration-none" to="/subjects">Subjects</Link>
            <Link className="p-4 text-white text-decoration-none" to="/professors">Professors</Link>
            <Link className="p-4 text-white text-decoration-none" to="/examination_periods">Examination Periods</Link>
            <Link className="p-4 text-white text-decoration-none" to="/exams">Exams</Link>
            <Link className="p-4 text-white text-decoration-none" to="/students">Students</Link>
            <Link className="p-4 text-white text-decoration-none" to="/exam_grading">Exam Grading</Link>
        </ul>
        </nav>
    );
};

export default Nav;