import React from 'react';

const Paginator = (props:{page:number,lastPage:number,size:number,pageChanged:(page:number,size:number)=>void}) => {
   

  const next = () =>{
    if(props.page <=props.lastPage-1){
        props.pageChanged(props.page+1,props.size);
    }
    
}
const previous = () =>{
    if(props.page > 0){
        props.pageChanged(props.page-1,props.size)
    }
    
}
    const size = (e:any)=>{
      props.pageChanged(props.page,Number(e.target.value))
    
      //After changeing the size of the table, allways return to the first page.
      //If size is bigger then the amount of data. 2 page will appear with no data

    }
    return (
        <nav>
        <ul className="pagination">
          <li className="page-item">
            <a href="#" className="page-link" onClick={previous} >Previous</a>
          </li>
          <li className="page-item">
          <a href="#" className="page-link" >{props.page+1}</a>
          </li>
          <li className="page-item">
            <a href="#" className="page-link" onClick={next}>Next</a>
          </li>
          <li>
          <select className="form-select" onChange={size}>
                <option value={2} >2</option>
                <option value={4}>4</option>
                <option value={8}>8</option>
                <option value={16}>16</option>
            </select>
          </li>
        </ul>
    </nav>
    );
};

export default Paginator;