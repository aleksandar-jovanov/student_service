import React from 'react';
import Nav from './Nav';

const Wrapper = (props:any) => {
    return (
        <>
        <Nav/>
        <div className="container-fluid" style={{paddingRight:"0", paddingLeft:"0"}} >
            <div className="row">
                

                <main className="ms-sm-auto  ">
 
                    {props.children}
  
                </main>
            </div>
        </div>
    </>   
    );
};

export default Wrapper;