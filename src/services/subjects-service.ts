import axios from "axios"
import { Subject } from "../models"
import { Page } from "../models/page.dto";

import {fetchSubjectsPending,fetchSubjectsSuccess,fetchSubjectsError} from '../actions/subjectActions'
import { useDispatch } from "react-redux";

function fetchSubjects(page:number,size:number) : Page<Subject[]> | any{
  return (dispatch: (arg0: { type: string; subjects?: Page<Subject[]>; error?: Error; }) => void)  =>  {
      dispatch(fetchSubjectsPending());
      axios.get<Page<Subject[]>>(`subject/page?page=${page}&size=${size}`)
      .then(res => {
        console.log(res);
        return res;
      })
      .then(res => {
          if(res.status === 200) {
              throw(res.status);
          }
          dispatch(fetchSubjectsSuccess(res.data));
          return res.data.content;
      })
      .catch(error => {
          dispatch(fetchSubjectsError(error));
      })
  } 
}

export default fetchSubjects;


// const getSubject =  async (page:number,size:number) =>{
//   return   await (await axios.get<Page<Subject[]>>(`subject/page?page=${page}&size=${size}`)).data
  
// };

// export {getSubject}