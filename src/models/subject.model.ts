export class Subject{
  constructor(
 public id?:number,
 public name?:string,
 public description?:string,
 public noOfEsp?:number,
 public yearOfStudy?:number,
 public semester?:"Winter"|"Summer"
  ){

  }
}
