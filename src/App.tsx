import React from 'react';

import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import Main from './pages/main';

import Subjects from './pages/Subjects/Subjects';
import ExaminationPeriods from './pages/Examination Periods/ExaminationPeriods';

import Students from './pages/Students/Students';
import ExamGrading from './pages/Exam Grading/ExamGrading';
import Professors from './pages/Professors/Professors';
import Exams from './pages/Exams/Exams';



function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Route path={'/'} exact component={Main}/>
        <Route path={'/subjects'} exact component={Subjects}/>
        <Route path={'/professors'} exact component={Professors}/>
        <Route path={'/examination_periods'} exact component={ExaminationPeriods}/>
        <Route path={'/exams'} exact component={Exams}/>
        <Route path={'/students'} exact component={Students}/>
        <Route path={'/exam_grading'} exact component={ExamGrading}/>
      </BrowserRouter>

    </div>
  );
}

export default App;
